from pathlib import Path

def check_existence(file_name):
    fsize = Path(file_name).stat().st_size/1024
    print(f"{file_name} has {fsize} KB in size.")


def main():
    file_name = './img/Encrypted-DSC03360.jpeg'
    check_existence(file_name)

